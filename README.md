# Rocket Chat Deployment - README and Documentation

## Overview

This repository contains the code for infrastructure and application delivery based on the test task provided by Ericsson. The deployment process utilizes Terraform for infrastructure provisioning, Ansible for application deployment, and GitLab CI/CD for automation.

### Folder Structure

1. **Terraform Directory:**
   - Contains Terraform code for provisioning infrastructure on-demand.

2. **deploy.yaml:**
   - Used by Ansible to execute necessary actions during the Rocket Chat deployment.

3. **compose.yaml:**
   - Deployment configuration file adhering to the official Rocket Chat documentation.

4. **.env:**
   - Custom configuration file specifying the Rocket Chat version (6.4.8) for deployment.

5. **.gitlab-ci.yml:**
   - CI/CD pipeline script automating the entire application and infrastructure delivery process.

### Configurations

AWS access, secret, and SSH private keys for the CI/CD process are configured as variables in the CI/CD section within project settings.

## Tech Stack

- **GitLab:** Source Code Management (SCM)
- **Terraform:** Infrastructure as Code (IaC)
- **Shell Scripting:** Automation
- **Ansible:** Deployment
- **Linux:** Platform
- **Docker:** Containerization
- **GitLab CI:** CI/CD tool

## Environment Setup

1. **CI/CD Configuration:**
   - Configure AWS access, secret, and SSH private keys as variables in the CI/CD section of project settings.

2. **Tech Stack:**
   - Utilizes GitLab CI as the CI/CD environment, integrating all necessary tools mentioned above.

## Deployment Process

### 1. Setup Environment for Deployment

The deployment environment is orchestrated through GitLab CI using the specified variables. The following tech stack is used for the deployment:

- GitLab (SCM)
- Terraform (IaC)
- Shell Scripting (Automation)
- Ansible (Deployment)
- Linux (Platform)
- Docker (Containerization)
- GitLab CI (CI/CD tool)

### 2. Running Deployment

The `.gitlab-ci.yml` file in the repository automates the CI/CD process. The process consists of two stages:

#### a. Infrastructure Delivery
- Checks for changes in the infrastructure using the state file.
- If changes are detected, it updates the infrastructure accordingly.
- If no changes are found, it creates an EC2 instance and a security group, installing Docker and Docker Compose using user data script.
- The public IP of the EC2 is written to an `inventory.ini` file for use in the deploy stage.
- Utilizes Terraform for this stage.

#### b. Application Delivery
- Ansible is employed as the deployment tool.
- The tasks are defined in the `deploy.yml` file.
- Executes tasks on the newly deployed server.
- After completion, the application can be accessed at: `http://{public_IP}:3000`.

### 3. Remove Deployment and Cleanup Resources

To remove the deployment and clean up all resources, a branch named 'destroy' is created. Any change committed to this branch or triggering CI/CD in this branch initiates the removal process. The Terraform destroy command is configured exclusively for this branch.
