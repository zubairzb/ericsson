provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "Docker" {
  ami           = "ami-02fe204d17e0189fb" 
  instance_type = "t2.medium"
  key_name      = "zblgeu"     

  vpc_security_group_ids = [aws_security_group.instance.id]

  subnet_id = "subnet-0ad3fc8463018ad6a"

  tags = {
    Projects = "Ericsson"
  }

  user_data = <<-EOF
              #!/bin/bash
				sudo yum update -y
				sudo yum install docker -y
				sudo service docker start
				sudo usermod -a -G docker ec2-user
				sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
				sudo chmod +x /usr/local/bin/docker-compose
              EOF
}

resource "aws_security_group" "instance" {
  name        = "allow-all-ingress"
  description = "Allow all incoming traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
