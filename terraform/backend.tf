terraform {
  backend "s3" {
    bucket = "zubairericssontask"
    region = "eu-central-1"
    key = "terraform.tfstate"
  }
}
